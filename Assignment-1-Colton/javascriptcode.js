//Help I'm steppin' into the Twilight Zone
//the place is a madhouse
//feels like being cloned

// Nice work Colton!  Your application works nicely and functions as required.
// You appear to have everything named correctly and you also have the comments
// I needed to see.  Everything here look good.  Well done!
// 10/10

function loadProvinces() {
    var provArray = ["Nova Scotia","Quebec","Ontario","Alberta","British Columbia","Saskatchewan",
    "Manitoba","Newfoundland and Labrador","Prince Edward Island","Yukon","Nunavut","Northwest Territories"]

    var provOutput = "<option value=''>-Select-</option>"

    for(pIndex=0;pIndex<provArray.length;pIndex++) {
        provOutput+="<option name='province" + pIndex + "' value='" + provArray[pIndex] +
        "' >" + provArray[pIndex] + "</option>";
    }
    document.getElementById("cboProv").innerHTML = provOutput;
}

function validateForm() {

    if(document.getElementById("txtName").value==""){
        alert("Please enter your name.");
        document.getElementById("txtName").focus();
        return false;
    }
    if(document.getElementById("txtEmail").value==""){
        alert("Please enter your Email.");
        document.getElementById("txtEmail").focus();
        return false;
    }
    if(document.getElementById("cboProv").selectedIndex==0){
        alert("Please select a province.");
        document.getElementById("cboProv").focus();
        return false;
    }
    document.forms[0].submit();
}
